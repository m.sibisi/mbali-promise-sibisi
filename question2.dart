//Create an array to store all the winning apps of the MTN Business App of the Year Awards since 2012;
//a) Sort and print the apps by name;
//b) Print the winning app of 2017 and the winning app of 2018.;
//c) the Print total number of apps from the array.

void main() {
  Map AppList = {
    'Ambani': 2021,
    'EasyEquities': 2020,
    'Naked Insurance': 2019,
    'Khula': 2018,
    'Standard Bank Shyft': 2017,
    'Domestly': 2016,
    'WumDrop': 2015,
    'Live Inspect': 2014,
    'SnapScan': 2013,
    'FNB Banking': 2012
  };
  print('*' * 100);
  print('Winning Apps From 2021 to 2012 :\n');
  AppList.forEach((name, year) {
    print('-$name');
  });
  print('*' * 100 + '\n');
  AppList.forEach((name, year) {
    if (year == 2017 || year == 2018) {
      print('$name is the Winner App of $year');
    }
  });
}
