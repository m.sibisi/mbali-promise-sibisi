//Create a class and
//a) then use an object to print the name of the app, sector/category, developer,
//and the year it won MTN Business App of the Year Awards.
// b) Create a function inside the class, transform the app name to all capital letters and then print the output.
void main() {
  Apps obj = new Apps('', '', '', 0);
  obj._AppName = 'fnb banking';
  obj._Sector = 'best solution Banking app';
  obj._Developer = 'Unknown';
  obj._Year = 2012;
  String name = obj.upperCase(obj._AppName);

  print(name);
  print(obj.message());
}

class Apps {
  String _AppName = '';
  String _Sector = '';
  String _Developer = '';
  int _Year = 0;

  Apps(String AppName, String Sector, String Developer, int Year) {
    this._AppName = AppName;
    this._Sector = Sector;
    this._Developer = Developer;
    this._Year = Year;
  }
  String upperCase(String name) {
    name = _AppName.toUpperCase();
    return name;
  }

  String message() {
    String message =
        '${upperCase(this._AppName)} application has been developed by ${this._Developer}, and was announced as the ${this._Sector} of MTN app of the year edition ${this._Year}.';
    return message;
  }
}
