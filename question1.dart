//Write a basic program that stores and then prints the following data:
// Your name, favorite app, and city;

void main() {
  final fullName = 'Mbali Promise Sibisi';
  final city = 'Johannesburg';
  final favoriteApp = 'Fnb';
  String message =
      'My name is $fullName.\nI live in $city, and my favorite App is $favoriteApp.';
  print(message);
}
